ORIGIN        This file is based on BMRB entry bmr6085, PDB code 1s6w.
DATE          Generated on 2012-08-22, original BMRB deposition date 2004-01-28.
MOLNAME       hepcidin
TEMPERATURE   298.0
pH            4.7
IONICSTRENGTH 0.000
LABELLING      (original '.')
EXPTYPE       NMR

    
CORRECTION    C (aliphatic)                   -0.766 +/- 0.144
CORRECTION    C (high ppm, proton attached)   0.560 +/- 0.237 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    C (high ppm, no proton)         -0.766 +/- 0.144 (correction based on aliphatic)
CORRECTION    N                               -1.837 +/- 1.056 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    H                               0.019 +/- 0.020 (NOT APPLIED, TOO UNCERTAIN)

MODEL 1
ATOM      1 C    GLY A   1     -11.743  -4.344   0.637  C Coil         
ATOM      2 CA   GLY A   1     -13.030  -5.173   0.549  C Coil          42.334         
ATOM      3 H1   GLY A   1     -12.526  -6.631   1.947  C Coil         
ATOM      4 H2   GLY A   1     -14.191  -6.340   1.822  C Coil         
ATOM      5 H3   GLY A   1     -13.196  -5.228   2.627  C Coil         
ATOM      6 HA2  GLY A   1     -12.946  -5.892  -0.254  C Coil           3.910   3.850 
ATOM      7 HA3  GLY A   1     -13.864  -4.518   0.357  C Coil           3.910   3.850 
ATOM      8 N    GLY A   1     -13.251  -5.896   1.834  C Coil         
ATOM      9 O    GLY A   1     -11.753  -3.215   1.091  C Coil         
ATOM     10 C    CYS A   2      -8.479  -4.532  -0.955  C Coil         
ATOM     11 CA   CYS A   2      -9.342  -4.138   0.253  C Coil          55.234         
ATOM     12 CB   CYS A   2      -8.671  -4.574   1.562  C Coil          46.234         
ATOM     13 H    CYS A   2     -10.652  -5.795  -0.163  C Coil           8.910         
ATOM     14 HA   CYS A   2      -9.519  -3.070   0.261  C Coil           5.580         
ATOM     15 HB2  CYS A   2      -9.395  -5.062   2.196  C Coil           3.040   2.690 
ATOM     16 HB3  CYS A   2      -7.863  -5.265   1.347  C Coil           3.040   2.690 
ATOM     17 N    CYS A   2     -10.637  -4.890   0.205  C Coil         
ATOM     18 O    CYS A   2      -8.689  -5.571  -1.559  C Coil         
ATOM     19 SG   CYS A   2      -8.005  -3.120   2.418  C Coil         
ATOM     20 C    ARG A   3      -5.171  -3.914  -2.117  E Strand       
ATOM     21 CA   ARG A   3      -6.649  -4.073  -2.479  E Strand        53.934         
ATOM     22 CB   ARG A   3      -7.041  -3.086  -3.587  E Strand        32.534         
ATOM     23 CD   ARG A   3      -7.941  -4.740  -5.238  E Strand        42.634         
ATOM     24 CG   ARG A   3      -6.821  -3.728  -4.954  E Strand        25.534         
ATOM     25 CZ   ARG A   3     -10.029  -4.568  -6.469  E Strand       
ATOM     26 H    ARG A   3      -7.355  -2.894  -0.814  E Strand         8.650         
ATOM     27 HA   ARG A   3      -6.845  -5.082  -2.794  E Strand         4.680         
ATOM     28 HB2  ARG A   3      -8.076  -2.804  -3.484  E Strand         1.790   1.750 
ATOM     29 HB3  ARG A   3      -6.421  -2.207  -3.511  E Strand         1.790   1.750 
ATOM     30 HD2  ARG A   3      -7.597  -5.506  -5.919  E Strand         2.990   2.910 
ATOM     31 HD3  ARG A   3      -8.290  -5.181  -4.319  E Strand         2.990   2.910 
ATOM     32 HE   ARG A   3      -9.001  -2.970  -5.841  E Strand         6.930         
ATOM     33 HG2  ARG A   3      -6.826  -2.952  -5.710  E Strand         1.610   1.290 
ATOM     34 HG3  ARG A   3      -5.867  -4.231  -4.964  E Strand         1.610   1.290 
ATOM     35 HH11 ARG A   3      -9.417  -6.426  -5.957  E Strand       
ATOM     36 HH12 ARG A   3     -10.837  -6.353  -6.958  E Strand       
ATOM     37 HH21 ARG A   3     -10.951  -2.868  -7.045  E Strand       
ATOM     38 HH22 ARG A   3     -11.723  -4.350  -7.511  E Strand       
ATOM     39 N    ARG A   3      -7.515  -3.726  -1.312  E Strand       120.700         
ATOM     40 NE   ARG A   3      -9.034  -3.951  -5.865  E Strand       
ATOM     41 NH1  ARG A   3     -10.106  -5.885  -6.449  E Strand       
ATOM     42 NH2  ARG A   3     -10.972  -3.875  -7.052  E Strand       
ATOM     43 O    ARG A   3      -4.820  -3.161  -1.227  E Strand       
ATOM     44 C    PHE A   4      -2.254  -3.299  -3.257  E Strand       
ATOM     45 CA   PHE A   4      -2.843  -4.505  -2.515  E Strand        56.534         
ATOM     46 CB   PHE A   4      -2.231  -5.815  -3.032  E Strand        38.834         
ATOM     47 CD1  PHE A   4      -1.242  -6.254  -0.747  E Strand       131.800         
ATOM     48 CD2  PHE A   4       0.123  -6.647  -2.709  E Strand       131.800         
ATOM     49 CE1  PHE A   4      -0.186  -6.664   0.072  E Strand       131.000         
ATOM     50 CE2  PHE A   4       1.183  -7.056  -1.888  E Strand       131.000         
ATOM     51 CG   PHE A   4      -1.088  -6.246  -2.139  E Strand       
ATOM     52 CZ   PHE A   4       1.026  -7.065  -0.496  E Strand       130.000         
ATOM     53 H    PHE A   4      -4.622  -5.210  -3.525  E Strand         8.830         
ATOM     54 HA   PHE A   4      -2.678  -4.413  -1.455  E Strand         4.940         
ATOM     55 HB2  PHE A   4      -2.982  -6.592  -3.041  E Strand         3.040   2.930 
ATOM     56 HB3  PHE A   4      -1.859  -5.663  -4.032  E Strand         3.040   2.930 
ATOM     57 HD1  PHE A   4      -2.177  -5.947  -0.303  E Strand         7.080         
ATOM     58 HD2  PHE A   4       0.238  -6.638  -3.783  E Strand         7.080         
ATOM     59 HE1  PHE A   4      -0.303  -6.670   1.148  E Strand         7.280         
ATOM     60 HE2  PHE A   4       2.116  -7.368  -2.329  E Strand         7.280         
ATOM     61 HZ   PHE A   4       1.842  -7.382   0.140  E Strand         7.210         
ATOM     62 N    PHE A   4      -4.306  -4.613  -2.809  E Strand       
ATOM     63 O    PHE A   4      -2.518  -3.082  -4.426  E Strand       
ATOM     64 C    CYS A   5       0.619  -1.196  -2.928  E TurnIV       
ATOM     65 CA   CYS A   5      -0.874  -1.296  -3.253  E TurnIV        53.134         
ATOM     66 CB   CYS A   5      -1.640  -0.097  -2.676  E TurnIV        40.734         
ATOM     67 H    CYS A   5      -1.275  -2.689  -1.634  E TurnIV         8.810         
ATOM     68 HA   CYS A   5      -1.023  -1.350  -4.320  E TurnIV         4.790         
ATOM     69 HB2  CYS A   5      -1.222   0.824  -3.069  E TurnIV         3.530   3.160 
ATOM     70 HB3  CYS A   5      -2.679  -0.167  -2.954  E TurnIV         3.530   3.160 
ATOM     71 N    CYS A   5      -1.469  -2.502  -2.585  E TurnIV       
ATOM     72 O    CYS A   5       1.060  -1.639  -1.885  E TurnIV       
ATOM     73 SG   CYS A   5      -1.499  -0.099  -0.869  E TurnIV       
ATOM     74 C    CYS A   6       3.271   0.972  -3.402  T TurnIV       
ATOM     75 CA   CYS A   6       2.870  -0.500  -3.538  T TurnIV        54.434         
ATOM     76 CB   CYS A   6       3.559  -1.153  -4.745  T TurnIV        45.734         
ATOM     77 H    CYS A   6       1.031  -0.272  -4.644  T TurnIV         8.800         
ATOM     78 HA   CYS A   6       3.128  -1.031  -2.641  T TurnIV         5.100         
ATOM     79 HB2  CYS A   6       3.454  -0.515  -5.611  T TurnIV         3.030   2.770 
ATOM     80 HB3  CYS A   6       4.612  -1.292  -4.528  T TurnIV         3.030   2.770 
ATOM     81 N    CYS A   6       1.403  -0.622  -3.808  T TurnIV       
ATOM     82 O    CYS A   6       4.325   1.382  -3.851  T TurnIV       
ATOM     83 SG   CYS A   6       2.792  -2.760  -5.077  T TurnIV       
ATOM     84 C    ASN A   7       1.764   3.879  -1.625  T TurnIV       
ATOM     85 CA   ASN A   7       2.771   3.222  -2.588  T TurnIV        54.034         
ATOM     86 CB   ASN A   7       2.673   3.848  -3.987  T TurnIV        36.434         
ATOM     87 CG   ASN A   7       3.776   4.894  -4.162  T TurnIV       
ATOM     88 H    ASN A   7       1.602   1.412  -2.409  T TurnIV         8.790         
ATOM     89 HA   ASN A   7       3.778   3.326  -2.208  T TurnIV         4.510         
ATOM     90 HB2  ASN A   7       2.781   3.081  -4.738  T TurnIV         3.010   2.780 
ATOM     91 HB3  ASN A   7       1.713   4.329  -4.098  T TurnIV         3.010   2.780 
ATOM     92 HD21 ASN A   7       4.040   4.518  -6.094  T TurnIV       
ATOM     93 HD22 ASN A   7       5.032   5.731  -5.445  T TurnIV       
ATOM     94 N    ASN A   7       2.441   1.770  -2.771  T TurnIV       
ATOM     95 ND2  ASN A   7       4.330   5.060  -5.330  T TurnIV       
ATOM     96 O    ASN A   7       1.127   4.861  -1.952  T TurnIV       
ATOM     97 OD1  ASN A   7       4.142   5.570  -3.222  T TurnIV       
ATOM     98 C    CYS A   8       1.389   4.791   1.550  T TurnIV       
ATOM     99 CA   CYS A   8       0.660   3.932   0.542  T TurnIV        56.034         
ATOM    100 CB   CYS A   8       0.048   2.740   1.304  T TurnIV        41.834         
ATOM    101 H    CYS A   8       2.146   2.554  -0.202  T TurnIV         8.000         
ATOM    102 HA   CYS A   8      -0.115   4.493   0.042  T TurnIV         5.100         
ATOM    103 HB2  CYS A   8      -0.888   3.049   1.752  T TurnIV         3.290         
ATOM    104 HB3  CYS A   8      -0.138   1.929   0.616  T TurnIV         3.290         
ATOM    105 N    CYS A   8       1.621   3.343  -0.445  T TurnIV       
ATOM    106 O    CYS A   8       0.867   5.769   2.045  T TurnIV       
ATOM    107 SG   CYS A   8       1.200   2.173   2.620  T TurnIV       
ATOM    108 C    CYS A   9       4.813   4.971   2.713  T TurnIV       
ATOM    109 CA   CYS A   9       3.305   5.054   2.989  T TurnIV        54.334         
ATOM    110 CB   CYS A   9       2.910   4.234   4.233  T TurnIV        40.534         
ATOM    111 H    CYS A   9       2.919   3.525   1.556  T TurnIV         8.820         
ATOM    112 HA   CYS A   9       2.978   6.074   3.092  T TurnIV         5.040         
ATOM    113 HB2  CYS A   9       3.581   3.392   4.336  T TurnIV         3.250   3.040 
ATOM    114 HB3  CYS A   9       2.985   4.858   5.113  T TurnIV         3.250   3.040 
ATOM    115 N    CYS A   9       2.560   4.359   1.927  T TurnIV       
ATOM    116 O    CYS A   9       5.308   3.934   2.308  T TurnIV       
ATOM    117 SG   CYS A   9       1.178   3.624   4.061  T TurnIV       
ATOM    118 C    PRO A  10       7.744   5.327   3.737  T TurnIV       
ATOM    119 CA   PRO A  10       6.966   6.110   2.662  T TurnIV        62.834         
ATOM    120 CB   PRO A  10       7.298   7.601   2.713  T TurnIV        30.934         
ATOM    121 CD   PRO A  10       4.987   7.359   3.399  T TurnIV        50.134         
ATOM    122 CG   PRO A  10       6.225   8.207   3.562  T TurnIV        26.634         
ATOM    123 HA   PRO A  10       7.186   5.720   1.682  T TurnIV         4.230         
ATOM    124 HB2  PRO A  10       8.269   7.751   3.167  T TurnIV         2.250   1.800 
ATOM    125 HB3  PRO A  10       7.273   8.029   1.724  T TurnIV         2.250   1.800 
ATOM    126 HD2  PRO A  10       4.483   7.238   4.349  T TurnIV         3.730         
ATOM    127 HD3  PRO A  10       4.322   7.790   2.666  T TurnIV         3.730         
ATOM    128 HG2  PRO A  10       6.537   8.208   4.599  T TurnIV         1.960   1.920 
ATOM    129 HG3  PRO A  10       6.019   9.216   3.238  T TurnIV         1.960   1.920 
ATOM    130 N    PRO A  10       5.498   6.069   2.919  T TurnIV       
ATOM    131 O    PRO A  10       8.639   5.849   4.377  T TurnIV       
ATOM    132 C    ASN A  11       8.079   1.764   4.610  T TurnIV       
ATOM    133 CA   ASN A  11       8.133   3.260   4.960  T TurnIV        53.734         
ATOM    134 CB   ASN A  11       7.390   3.527   6.273  T TurnIV        38.734         
ATOM    135 CG   ASN A  11       8.358   4.094   7.313  T TurnIV       
ATOM    136 H    ASN A  11       6.692   3.671   3.404  T TurnIV         8.830         
ATOM    137 HA   ASN A  11       9.157   3.586   5.047  T TurnIV         4.320         
ATOM    138 HB2  ASN A  11       6.594   4.238   6.098  T TurnIV         3.020   2.920 
ATOM    139 HB3  ASN A  11       6.971   2.601   6.641  T TurnIV         3.020   2.920 
ATOM    140 HD21 ASN A  11       8.046   5.990   6.796  T TurnIV       
ATOM    141 HD22 ASN A  11       9.151   5.747   8.063  T TurnIV       
ATOM    142 N    ASN A  11       7.414   4.076   3.933  T TurnIV       
ATOM    143 ND2  ASN A  11       8.533   5.382   7.397  T TurnIV       
ATOM    144 O    ASN A  11       8.188   0.918   5.478  T TurnIV       
ATOM    145 OD1  ASN A  11       8.972   3.352   8.055  T TurnIV       
ATOM    146 C    MET A  12       8.299  -0.185   1.500  T TurnIV       
ATOM    147 CA   MET A  12       7.851  -0.012   2.957  T TurnIV        54.834         
ATOM    148 CB   MET A  12       6.377  -0.400   3.114  T TurnIV        34.034         
ATOM    149 CE   MET A  12       3.633  -2.082   3.005  T TurnIV        16.634         
ATOM    150 CG   MET A  12       6.274  -1.847   3.606  T TurnIV        31.234         
ATOM    151 H    MET A  12       7.829   2.118   2.666  T TurnIV         7.590         
ATOM    152 HA   MET A  12       8.462  -0.612   3.614  T TurnIV         4.500         
ATOM    153 HB2  MET A  12       5.907   0.258   3.830  T TurnIV         2.020   1.790 
ATOM    154 HB3  MET A  12       5.877  -0.309   2.160  T TurnIV         2.020   1.790 
ATOM    155 HE1  MET A  12       3.741  -3.023   2.480  T TurnIV         2.000         
ATOM    156 HE2  MET A  12       3.917  -1.275   2.348  T TurnIV         2.000         
ATOM    157 HE3  MET A  12       2.604  -1.950   3.311  T TurnIV         2.000         
ATOM    158 HG2  MET A  12       6.330  -2.520   2.762  T TurnIV         2.430   2.370 
ATOM    159 HG3  MET A  12       7.089  -2.055   4.285  T TurnIV         2.430   2.370 
ATOM    160 N    MET A  12       7.912   1.427   3.352  T TurnIV       119.000         
ATOM    161 O    MET A  12       7.637   0.261   0.583  T TurnIV       
ATOM    162 SD   MET A  12       4.701  -2.083   4.468  T TurnIV       
ATOM    163 C    SER A  13       9.192  -2.251  -0.751  C GammaInv     
ATOM    164 CA   SER A  13       9.902  -1.043  -0.117  C GammaInv      56.934         
ATOM    165 CB   SER A  13      11.410  -1.303   0.015  C GammaInv      62.434         
ATOM    166 H    SER A  13       9.932  -1.185   2.034  C GammaInv       8.700         
ATOM    167 HA   SER A  13       9.737  -0.156  -0.714  C GammaInv       4.650         
ATOM    168 HB2  SER A  13      11.721  -2.008  -0.741  C GammaInv       3.860   3.810 
ATOM    169 HB3  SER A  13      11.946  -0.370  -0.124  C GammaInv       3.860   3.810 
ATOM    170 HG   SER A  13      12.088  -2.713   1.183  C GammaInv     
ATOM    171 N    SER A  13       9.413  -0.832   1.281  C GammaInv     121.200         
ATOM    172 O    SER A  13       9.814  -3.233  -1.111  C GammaInv     
ATOM    173 OG   SER A  13      11.695  -1.841   1.306  C GammaInv     
ATOM    174 C    GLY A  14       5.677  -2.939  -1.675  C GammaInv     
ATOM    175 CA   GLY A  14       7.147  -3.326  -1.497  C GammaInv      43.534         
ATOM    176 H    GLY A  14       7.409  -1.387  -0.591  C GammaInv       8.290         
ATOM    177 HA2  GLY A  14       7.577  -3.561  -2.460  C GammaInv       4.160   4.020 
ATOM    178 HA3  GLY A  14       7.217  -4.188  -0.851  C GammaInv       4.160   4.020 
ATOM    179 N    GLY A  14       7.894  -2.186  -0.888  C GammaInv     111.400         
ATOM    180 O    GLY A  14       5.360  -1.904  -2.230  C GammaInv     
ATOM    181 C    CYS A  15       2.589  -3.764  -0.034  C Coil         
ATOM    182 CA   CYS A  15       3.326  -3.444  -1.338  C Coil          54.534         
ATOM    183 CB   CYS A  15       2.827  -4.335  -2.477  C Coil          46.034         
ATOM    184 H    CYS A  15       5.061  -4.581  -0.752  C Coil           8.660         
ATOM    185 HA   CYS A  15       3.191  -2.409  -1.598  C Coil           5.370         
ATOM    186 HB2  CYS A  15       2.897  -5.370  -2.178  C Coil           3.130   2.720 
ATOM    187 HB3  CYS A  15       1.797  -4.094  -2.699  C Coil           3.130   2.720 
ATOM    188 N    CYS A  15       4.780  -3.758  -1.202  C Coil         120.100         
ATOM    189 O    CYS A  15       3.003  -4.609   0.732  C Coil         
ATOM    190 SG   CYS A  15       3.844  -4.068  -3.956  C Coil         
ATOM    191 C    GLY A  16      -0.769  -3.389   1.165  E Strand       
ATOM    192 CA   GLY A  16       0.729  -3.342   1.474  E Strand        44.534         
ATOM    193 H    GLY A  16       1.187  -2.407  -0.418  E Strand         8.320         
ATOM    194 HA2  GLY A  16       1.039  -4.287   1.897  E Strand         3.130   2.720 
ATOM    195 HA3  GLY A  16       0.922  -2.551   2.181  E Strand         3.130   2.720 
ATOM    196 N    GLY A  16       1.500  -3.089   0.220  E Strand       111.900         
ATOM    197 O    GLY A  16      -1.181  -3.274   0.023  E Strand       
ATOM    198 C    VAL A  17      -3.649  -2.198   1.915  E Strand       
ATOM    199 CA   VAL A  17      -3.060  -3.616   1.950  E Strand        62.234         
ATOM    200 CB   VAL A  17      -3.616  -4.411   3.140  E Strand        31.534         
ATOM    201 CG1  VAL A  17      -5.107  -4.691   2.924  E Strand        21.834  21.634 
ATOM    202 CG2  VAL A  17      -2.870  -5.744   3.269  E Strand        21.834  21.634 
ATOM    203 H    VAL A  17      -1.226  -3.642   3.081  E Strand         8.520         
ATOM    204 HA   VAL A  17      -3.279  -4.134   1.029  E Strand         4.350         
ATOM    205 HB   VAL A  17      -3.486  -3.834   4.046  E Strand         1.940         
ATOM    206 HG11 VAL A  17      -5.681  -4.185   3.685  E Strand         1.050   0.900 
ATOM    207 HG12 VAL A  17      -5.285  -5.755   2.987  E Strand         1.050   0.900 
ATOM    208 HG13 VAL A  17      -5.404  -4.334   1.950  E Strand         1.050   0.900 
ATOM    209 HG21 VAL A  17      -2.544  -6.073   2.292  E Strand         1.050   0.900 
ATOM    210 HG22 VAL A  17      -3.530  -6.485   3.695  E Strand         1.050   0.900 
ATOM    211 HG23 VAL A  17      -2.010  -5.617   3.911  E Strand         1.050   0.900 
ATOM    212 N    VAL A  17      -1.585  -3.555   2.174  E Strand       120.700         
ATOM    213 O    VAL A  17      -3.414  -1.392   2.796  E Strand       
ATOM    214 C    CYS A  18      -6.561  -0.668   0.738  E Strand       
ATOM    215 CA   CYS A  18      -5.033  -0.542   0.785  E Strand        52.934         
ATOM    216 CB   CYS A  18      -4.509   0.025  -0.536  E Strand        39.634         
ATOM    217 H    CYS A  18      -4.588  -2.575   0.207  E Strand         9.250         
ATOM    218 HA   CYS A  18      -4.725   0.086   1.606  E Strand         5.110         
ATOM    219 HB2  CYS A  18      -4.253  -0.787  -1.202  E Strand         3.110   2.850 
ATOM    220 HB3  CYS A  18      -5.274   0.634  -0.993  E Strand         3.110   2.850 
ATOM    221 N    CYS A  18      -4.414  -1.899   0.902  E Strand       127.200         
ATOM    222 O    CYS A  18      -7.094  -1.458  -0.019  E Strand       
ATOM    223 SG   CYS A  18      -3.041   1.040  -0.224  E Strand       
ATOM    224 C    CYS A  19      -9.372   1.388   1.159  C Coil         
ATOM    225 CA   CYS A  19      -8.762   0.026   1.529  C Coil          54.734         
ATOM    226 CB   CYS A  19      -9.145  -0.357   2.965  C Coil          38.734         
ATOM    227 H    CYS A  19      -6.813   0.732   2.126  C Coil           9.300         
ATOM    228 HA   CYS A  19      -9.099  -0.732   0.842  C Coil           5.420         
ATOM    229 HB2  CYS A  19      -8.300  -0.205   3.620  C Coil           3.080   2.990 
ATOM    230 HB3  CYS A  19      -9.968   0.263   3.293  C Coil           3.080   2.990 
ATOM    231 N    CYS A  19      -7.265   0.102   1.530  C Coil         128.300         
ATOM    232 O    CYS A  19     -10.566   1.577   1.257  C Coil         
ATOM    233 SG   CYS A  19      -9.643  -2.100   3.015  C Coil         
ATOM    234 C    ARG A  20      -8.976   3.887  -1.160  C Coil         
ATOM    235 CA   ARG A  20      -9.095   3.680   0.355  C Coil          54.334         
ATOM    236 CB   ARG A  20      -8.209   4.686   1.101  C Coil          30.734         
ATOM    237 CD   ARG A  20      -8.992   5.957   3.111  C Coil          45.934         
ATOM    238 CG   ARG A  20      -8.484   4.606   2.610  C Coil          25.934         
ATOM    239 CZ   ARG A  20      -7.529   6.341   5.036  C Coil         
ATOM    240 H    ARG A  20      -7.606   2.153   0.654  C Coil           7.560         
ATOM    241 HA   ARG A  20     -10.121   3.783   0.671  C Coil           4.560         
ATOM    242 HB2  ARG A  20      -7.170   4.456   0.910  C Coil           1.770   1.590 
ATOM    243 HB3  ARG A  20      -8.424   5.684   0.747  C Coil           1.770   1.590 
ATOM    244 HD2  ARG A  20      -8.473   6.764   2.612  C Coil           3.110         
ATOM    245 HD3  ARG A  20     -10.054   6.037   2.944  C Coil           3.110         
ATOM    246 HE   ARG A  20      -9.424   5.739   5.210  C Coil           7.190         
ATOM    247 HG2  ARG A  20      -9.230   3.848   2.803  C Coil           1.440         
ATOM    248 HG3  ARG A  20      -7.572   4.351   3.128  C Coil           1.440         
ATOM    249 HH11 ARG A  20      -6.694   6.610   3.229  C Coil         
ATOM    250 HH12 ARG A  20      -5.670   6.941   4.579  C Coil         
ATOM    251 HH21 ARG A  20      -8.065   6.157   6.962  C Coil         
ATOM    252 HH22 ARG A  20      -6.438   6.674   6.686  C Coil         
ATOM    253 N    ARG A  20      -8.565   2.332   0.733  C Coil         
ATOM    254 NE   ARG A  20      -8.707   5.974   4.578  C Coil         
ATOM    255 NH1  ARG A  20      -6.556   6.651   4.216  C Coil         
ATOM    256 NH2  ARG A  20      -7.328   6.394   6.327  C Coil         
ATOM    257 O    ARG A  20      -7.887   3.864  -1.710  C Coil         
ATOM    258 C    PHE A  21     -11.068   5.344  -3.741  C Coil         
ATOM    259 CA   PHE A  21     -10.033   4.298  -3.316  C Coil          58.734         
ATOM    260 CB   PHE A  21     -10.371   2.932  -3.926  C Coil          40.234         
ATOM    261 CD1  PHE A  21      -8.115   1.907  -4.401  C Coil         129.400         
ATOM    262 CD2  PHE A  21      -9.402   1.070  -2.525  C Coil         129.400         
ATOM    263 CE1  PHE A  21      -7.094   0.999  -4.104  C Coil         131.300         
ATOM    264 CE2  PHE A  21      -8.381   0.163  -2.227  C Coil         131.300         
ATOM    265 CG   PHE A  21      -9.270   1.944  -3.612  C Coil         
ATOM    266 CZ   PHE A  21      -7.226   0.127  -3.017  C Coil         132.000         
ATOM    267 H    PHE A  21     -10.938   4.107  -1.368  C Coil           8.060         
ATOM    268 HA   PHE A  21      -9.042   4.602  -3.619  C Coil           4.430         
ATOM    269 HB2  PHE A  21     -11.304   2.577  -3.514  C Coil           3.260   2.750 
ATOM    270 HB3  PHE A  21     -10.467   3.034  -4.996  C Coil           3.260   2.750 
ATOM    271 HD1  PHE A  21      -8.012   2.581  -5.243  C Coil           7.270         
ATOM    272 HD2  PHE A  21     -10.293   1.099  -1.917  C Coil           7.270         
ATOM    273 HE1  PHE A  21      -6.200   0.972  -4.715  C Coil           7.310         
ATOM    274 HE2  PHE A  21      -8.484  -0.509  -1.389  C Coil           7.310         
ATOM    275 HZ   PHE A  21      -6.436  -0.572  -2.789  C Coil           7.270         
ATOM    276 N    PHE A  21     -10.078   4.089  -1.834  C Coil         
ATOM    277 O'   PHE A  21     -10.721   6.201  -4.531  C Coil         
ATOM    278 O''  PHE A  21     -12.189   5.272  -3.268  C Coil         
ENDMDL
